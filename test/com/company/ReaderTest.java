package com.company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class ReaderTest {

    // all tests follow the triple A method
    // arrange/act/assert

    // test for checking that the user's input is correctly received
    @Test
    void retrieveUserInputTest() {

        // Arrange
        var in = new ByteArrayInputStream("one plus two times three".getBytes());
        System.setIn(in);
        var scanner = new Scanner(System.in);
        var reader = new Reader(scanner);

        // Act
        reader.retrieveUserInput();

        // Assert
        Assertions.assertEquals(reader.getUserInput(), "one plus two times three");
    }

    // test for checking the return value
    @Test
    void retrieveUserInputReturnTest() {

        // Arrange
        var in = new ByteArrayInputStream("one plus two times three".getBytes());
        System.setIn(in);
        var scanner = new Scanner(System.in);
        var reader = new Reader(scanner);

        // Act
        var result = reader.retrieveUserInput();

        // Assert
        Assertions.assertEquals(result.getClass(), ArrayList.class);
        Assertions.assertFalse(result.isEmpty());
    }
}