package com.company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    //all tests follow the triple A method
    // arrange/act/assert

    // test to check the return value using multiplication
    @Test
    void calculateTheConvertedCollReturnUsingTimesTest() {

        // Arrange
        var in = new ByteArrayInputStream("one plus two times three".getBytes());
        System.setIn(in);
        var scanner = new Scanner(System.in);
        var reader = new Reader(scanner);
        var calculator = new Calculator();

        // Act
        var testCollection = reader.retrieveUserInput();
        calculator.calculateTheConvertedColl(testCollection);

        // Assert
        Assertions.assertTrue(calculator.getCalculationResult().equals(7));
    }

    // test to check the return value using division
    @Test
    void calculateTheConvertedCollReturnUsingDivisionTest() {

        // Arrange
        var in = new ByteArrayInputStream("one plus two times three divided-by two".getBytes());
        System.setIn(in);
        var scanner = new Scanner(System.in);
        var reader = new Reader(scanner);
        var calculator = new Calculator();

        // Act
        var testCollection = reader.retrieveUserInput();
        calculator.calculateTheConvertedColl(testCollection);

        // Assert
        Assertions.assertTrue(calculator.getCalculationResult().equals(4));
    }

    // test to check the return value using subtraction
    @Test
    void calculateTheConvertedCollReturnUsingSubtractionTest() {

        // Arrange
        var in = new ByteArrayInputStream("one plus two times three minus two".getBytes());
        System.setIn(in);
        var scanner = new Scanner(System.in);
        var reader = new Reader(scanner);
        var calculator = new Calculator();

        // Act
        var testCollection = reader.retrieveUserInput();
        calculator.calculateTheConvertedColl(testCollection);

        // Assert
        Assertions.assertTrue(calculator.getCalculationResult().equals(5));
    }

    // test to check the return value using subtraction
    @Test
    void calculateTheConvertedCollReturnUsingMultipleChainsTest() {

        // Arrange
        var in = new ByteArrayInputStream("one plus two times three minus two times eight divided-by four".getBytes());
        System.setIn(in);
        var scanner = new Scanner(System.in);
        var reader = new Reader(scanner);
        var calculator = new Calculator();

        // Act
        var testCollection = reader.retrieveUserInput();
        calculator.calculateTheConvertedColl(testCollection);

        // Assert
        Assertions.assertTrue(calculator.getCalculationResult().equals(3));
    }
}