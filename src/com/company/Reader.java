package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Reader {

    // class variables
    private Scanner scanner;
    private String userInput;

    // getters/setters
    public Scanner getScanner() {
        return scanner;
    }

    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }

    public String getUserInput() {
        return userInput;
    }

    public void setUserInput(String userInput) {
        this.userInput = userInput;
    }

    // constructor
    public Reader(Scanner scanner) {
        this.scanner = scanner;
    }

    // class methods
    public ArrayList<String> retrieveUserInput() {

        // ask user for their input
        System.out.println("Please enter a calculation:");
        var input = this.scanner.nextLine();
        this.setUserInput(input);

        // call next method
        return convertInput();
    }

    private ArrayList<String> convertInput() {

        // split the user's input
        var arrayOfWords = this.getUserInput().split(" ");

        // collection for converted words
        ArrayList<String> collOfConvertedWords = new ArrayList<String>();

        // check every word
        for (String word : arrayOfWords) {

            // switch to find the word
            switch (word.toLowerCase()) {

                // number cases
                case "zero":
                    collOfConvertedWords.add("0");
                    break;
                case "one":
                    collOfConvertedWords.add("1");
                    break;
                case "two":
                    collOfConvertedWords.add("2");
                    break;
                case "three":
                    collOfConvertedWords.add("3");
                    break;
                case "four":
                    collOfConvertedWords.add("4");
                    break;
                case "five":
                    collOfConvertedWords.add("5");
                    break;
                case "six":
                    collOfConvertedWords.add("6");
                    break;
                case "seven":
                    collOfConvertedWords.add("7");
                    break;
                case "eight":
                    collOfConvertedWords.add("8");
                    break;
                case "nine":
                    collOfConvertedWords.add("9");
                    break;
                case "ten":
                    collOfConvertedWords.add("10");
                    break;

                // operator cases
                case "plus":
                case "add":
                    collOfConvertedWords.add("+");
                    break;
                case "minus":
                case "subtract":
                case "less":
                    collOfConvertedWords.add("-");
                    break;
                case "multiplied-by":
                case "times":
                    collOfConvertedWords.add("*");
                    break;
                case "divided-by":
                case "over":
                    collOfConvertedWords.add("/");
                    break;
                default:
                    System.out.println("Invalid input. Try again with a natural language number e.g. one, two, three.");
                    System.exit(0);

            }
        }


        // return the collection
        return collOfConvertedWords;
    }

}
