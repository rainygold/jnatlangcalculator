/*
    Author: rainygold
    Last update: 01/04/2019
 */

package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // initialise any necessary objects
        Scanner scanner = new Scanner(System.in);
        var reader = new Reader(scanner);
        var calculator = new Calculator();

        // ask user for input and pass it to the calculator
        var userInput = reader.retrieveUserInput();
        calculator.calculateTheConvertedColl(userInput);

        // print result
        System.out.println(calculator.getCalculationResult());
    }
}
