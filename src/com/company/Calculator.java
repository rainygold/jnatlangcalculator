package com.company;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;

public class Calculator {
    private String calculationResult;

    public String getCalculationResult() {
        return calculationResult;
    }

    public void setCalculationResult(String calculationResult) {
        this.calculationResult = calculationResult;
    }

    // performs the desired calculation
    protected void calculateTheConvertedColl(ArrayList<String> collection) {

        // multiply takes precedence
        while (collection.contains("*")) {
            calculateViaOperator(collection, "*", "*");
        }

        // division takes second precedence
        while (collection.contains("/")) {
            calculateViaOperator(collection, "/", "/");
        }

        // calculate the rest
        for (String numberOrOperator : collection) {

            calculateViaOperator(collection, numberOrOperator, numberOrOperator);

        }

        // remove all the empty elements
        collection.removeAll(Collections.singleton(""));

        // set class variable to the overall result
        var result = collection.get(0);

        // format the result to only have two decimal places
        if (Float.parseFloat(result) % 1 == 0) {
            this.setCalculationResult(String.format("%.0f", Float.valueOf(result)));
        } else {
            DecimalFormat dfTwo = new DecimalFormat("#.##");
            this.setCalculationResult(String.valueOf(dfTwo.format(Float.parseFloat(result))));
        }
    }

    // iterates through the collection and performs a math operation depending on input
    private void calculateViaOperator(ArrayList<String> collection, String numberOrOperator, String operator) {

        String[] containerForNumbers = new String[2];
        int[] containerForIteratorNumbers = new int[2];
        int countForNumOne = 0;
        int countForNumTwo = 0;

        // skip over all the empty elements and find the numbers
        for (int i = collection.indexOf(numberOrOperator); i >= 0; i--) {
            if (!collection.get(i).equals("-") && !collection.get(i).equals("+") && !collection.get(i).equals("/") && !collection.get(i).equals("*")) {

                countForNumOne++;

                if (!collection.get(i).equals("")) {
                    containerForNumbers[0] = collection.get(i);
                    containerForIteratorNumbers[0] = countForNumOne;
                    break;
                }
            }
        }

        for (int x = collection.indexOf(numberOrOperator); x < collection.size(); x++) {
            if (!collection.get(x).equals("-") && !collection.get(x).equals("+") && !collection.get(x).equals("/") && !collection.get(x).equals("*")) {

                countForNumTwo++;

                if (!collection.get(x).equals("")) {
                    containerForNumbers[1] = collection.get(x);
                    containerForIteratorNumbers[1] = countForNumTwo;
                    break;
                }
            }
        }

        var firstNum = containerForNumbers[0];
        var secondNum = containerForNumbers[1];

        // perform the specific operation
        switch (operator) {
            case "+":
                float resultOfAddition = Float.parseFloat(firstNum) + Float.parseFloat(secondNum);
                int indexOfOriginalElement = collection.indexOf(numberOrOperator);

                // remove the now useless numbers from the collection and replace operator with result
                collection.set(indexOfOriginalElement, String.valueOf(resultOfAddition));
                collection.set(indexOfOriginalElement - containerForIteratorNumbers[0], "");
                collection.set(indexOfOriginalElement + containerForIteratorNumbers[1], "");
                break;
            case "*":
                float resultOfMultiply = Float.parseFloat(firstNum) * Float.parseFloat(secondNum);
                int indexOfOriginalMultiplyIndex = collection.indexOf(numberOrOperator);

                collection.set(indexOfOriginalMultiplyIndex, String.valueOf(resultOfMultiply));
                collection.set(indexOfOriginalMultiplyIndex - containerForIteratorNumbers[0], "");
                collection.set(indexOfOriginalMultiplyIndex + containerForIteratorNumbers[1], "");
                break;
            case "-":
                float resultOfSubtraction = Float.parseFloat(firstNum) - Float.parseFloat(secondNum);
                int indexOfOriginalSubtractIndex = collection.indexOf(numberOrOperator);

                collection.set(indexOfOriginalSubtractIndex, String.valueOf(resultOfSubtraction));
                collection.set(indexOfOriginalSubtractIndex - containerForIteratorNumbers[0], "");
                collection.set(indexOfOriginalSubtractIndex + containerForIteratorNumbers[1], "");
                break;
            case "/":
                float resultOfDivision = Float.parseFloat(firstNum) / Float.parseFloat(secondNum);
                int indexOfOriginalDivisionIndex = collection.indexOf(numberOrOperator);

                collection.set(indexOfOriginalDivisionIndex, String.valueOf(resultOfDivision));
                collection.set(indexOfOriginalDivisionIndex - containerForIteratorNumbers[0], "");
                collection.set(indexOfOriginalDivisionIndex + containerForIteratorNumbers[1], "");
                break;
        }


    }

}
