### Build
* IDE: Run Main in IntelliJ

### Tools
* OS: openSuse Tumbleweed
* Editor: IntelliJ Comm. 2019
* Java 11
* Testing: JUnit5

### Ethos
A simple natural language calculator that accepts chaining of commands.

Supports addition, subtraction, division, multiplication.

E.g. 

* one plus two
* two times four 
* six divided-by two
* nine minus three
* six times two plus four divided-by three

### Testing
Unit tests for each class.

No direct testing of private methods.

### Improvements
* For loop logic in calculateViaOperator could use streams to be more readable.
* A small help command could help users (Type 'help' and example commands will be given)